
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('churches').del()
    .then(function () {
      // Inserts seed entries
      return knex('churches').insert([
        { 
          name: 'Catedral de Nuestra Señora de la Asunción',
          location: '11.929933,-85.953148',
          direction: 'Parque Central de Granada',
          schedule_mass: 'Lunes a viernes: 7:00 AM, 5:00 PM Sábados y Domingos: 7:00 AM, 5:00 PM y 7:00 PM',
          construction_year: '1525',
          territorial_expansion: '---', 
          history:'El primer templo de la catedral fue construido alrededor de 1525 con Tapicel y rafaz de piedra, ladrillo, cal y techo de paja. Para 1578 la iglesia ya se había quemado en dos ocasiones. Para 1811 se eleva a parroquia compuesta por dos torres que se componían de cinco cuerpos. En el cuarto cuerpo de la torre de la izquierda estaban las campanas. En 1880 empezaron los trabajos de construcción para un nuevo templo, con planos elaborados por el Padre Jesuita Alejandro Nicolás Cáceres para sustituir a la parroquia construida. Esta vez, constando de tres naves. La catedral fue concluida en su totalidad en 1972 con una superficie total de 3,614.87 m².',
          banner: 'http://127.0.0.1:5000/uploads/catedral.jpg'
        },
        { 
          name: 'Xalteva',
          location: '11.928924,-85.960755',
          direction: 'Calle Real Xalteva, Granada',
          schedule_mass: 'Lunes a Sábado: 7:00 AM, 5:00 PM Domingos: 7:00 AM, 8:00 AM, 10:00 AM y 5:00 PM',
          construction_year: '1916',
          territorial_expansion: '---',
          history: 'La iglesia de Xalteva fue construida en la época colonial, sufrió severos daños en la guerra nacional, motivo por el cual tuvo que ser reconstruida por dentro y por fuera. En 1890 las paredes se deterioraron debido a un fuerte sismo, posterior a esto se realizó una considerable inversión para reparar los daños. Las obras estuvieron a cargo de Carlos Ferrey, la fachada se concluyó en 1895, las paredes fueron construidas con adobe, el piso de ladrillo de cemento, la estructura del techo es de madera y la cubierta de teja de barro, terminando la restauración en 1898.',
          banner: 'http://127.0.0.1:5000/uploads/xalteva.jpg'
        },
        { 
          name: 'Nuestra Señora de Guadalupe',
          location: '11.930373,-85.947542',
          direction: 'Calle La Calzada, Granada',
          schedule_mass: 'Lunes a Sábado: 7:00 AM, 5:00 PM Domingos: 8:00 AM, 10:00 AM y 4:00 PM',
          construction_year: '1624',
          territorial_expansion: '---',
          history: 'La Iglesia de Guadalupe: Fundada en 1624 - 1626 por Fray Benito de Baltodano. Luego que fue incendiada fue reconstruida con diferentes formas, lo que ha hecho que pierda un poco de su valor arqueológico. Se encuentra localizada sobre la calle La Calzada a poca distancia del Gran Lago. La Iglesia de Guadalupe tenía una posición estratégica en tiempo de la Colonia, ya que estaba a la entrada de la ciudad por la vía del gran Lago, por lo que se convirtió en la típica Iglesia Fortaleza, razón por la cual estuvo siempre expuesta a los saqueos y destrucciones de piratas y filibusteros.',
          banner: 'http://127.0.0.1:5000/uploads/guadalupe.jpg'          
        },
        { 
          name: 'La Merced',
          location: '11.929490,-85.956990',
          direction: 'Calle Real Xalteva, Granada',
          schedule_mass: 'Lunes a Sábado: 7:00 AM, 5:00 PM Domingos: 7:00 AM, 8:00 AM, 10:00 AM y 5:00 PM',
          construction_year: '1534',
          territorial_expansion: '---',
          history: 'Fundada y edificada por primera vez en 1534. En 1670 el Pirata Morgan la saquea e incendia, la fé de los granadinos se impuso y una vez más se reconstruye en 1853. Después es quemada por el filibustero William Walker en 1856, En 1862 el maestro Don Esteban Sandino finaliza su restauración con adornos neoclásico, y en su fachada es una rica variedad de estilo barroco. Una de sus principales atracciones es el retablo dorado del altar mayor. La torre de la iglesia fue construida en 1781 y en 1854 durante la guerra civil fue demolida hasta la mitad y restaurada en 1863.',
          banner: 'http://127.0.0.1:5000/uploads/merced.jpg'
        },
        { 
          name: 'San Francisco',
          location: '11.932009,-85.952055',
          direction: '---',
          schedule_mass:'Lunes a Sábado: 8:00 AM, 4:00 PM Domingos: 7:00 AM, 8:00 AM y 5:00 PM',
          construction_year: '1526',
          territorial_expansion: '---',
          history: 'El cronista Gonzalo Fernández de Oviedo afirma que Francisco Hernández de Córdoba al fundar Granada en 1524 levantó un suntuoso templo dedicado a San Francisco. Construido de paredes de madera y techo de paja. Aunque sea más probable que este templo coincidiera con la actual Catedral, como anotan distintos historiadores. Ya por el año de 1536 hay referencias sobre la Iglesia y el Convento, que fueron incendiados en 1685. Luego el templo se reconstruyó con un estilo románico de sólida construcción. Tanto la iglesia como el convento fueron dañados en 1856 por los combates que se dieron en ellos para desalojar a Walker, así como por el incendio que éste ordenara antes de abandonar la ciudad.  La Iglesia y el Convento fueron incendiados en 1665, y en 1685 por invasores piratas por lo que el Templo tuvo que ser reconstruido en sólido estilo Románico-Español.',
          banner: 'http://127.0.0.1:5000/uploads/sanfrancisco.jpg'
        },
        { 
          name: 'Capilla María Auxiliadora',
          location: '11.928308,-85.963720',
          direction: 'Calle Real Xalteva, Granada',
          schedule_mass: 'Lunes a viernes: 7:00 AM, 5:00 PM Sábados y Domingos: 7:00 AM, 8:00 AM y 5:00 PM',
          construction_year: '1526',
          territorial_expansion: '---',
          history: 'La Capilla María Auxiliadora es una iglesia pública actualmente regentada por la Comunidad Salesiana de Granada dentro del territorio parroquial de la Parroquia Nuestra Señora de la Asunción, Xalteva. Construida entre los años 1921 y 1922, inaugurada el 15 de abril de 1922; diseñada por el Sacerdote Salesiano José Misieri. Es el centro de devoción a María Auxiliadora para el departamento de Granada y la población que habita en esta región sureña del país, así como un referente mariano para toda Nicaragua y quienes se encuentran fuera del país.',
          banner: 'http://127.0.0.1:5000/uploads/ma.jpg'
        },
        { 
          name: 'Capilla de las Ánimas',
          location: '11.922510,-85.966816',
          direction: 'Cementero municipal',
          schedule_mass: 'Lunes a Sábado: 7:00 AM, 4:00 PM Domingos: 7:00 AM y 5:00 PM',
          construction_year: '1871',
          territorial_expansion: '---',
          history: 'En 1878 el norteamericano Teodoro E. Hocke emprendió una campaña con el fin de dotar al cementerio de Granada de una capilla y para ello había ideado una réplica de La Magdalena de Paris. Las obras fueron realizadas bajo la dirección del maestro Carlos Ferrey. En 1885 estaban levantadas las paredes, faltando únicamente el techo. La Junta Local de Beneficencia se empeñó en 1888 para que los trabajos no se discontinuaran, pero nuevamente decayeron y quedaron olvidados hasta que posteriormente, la misma Junta tomó a su cargo el remate de la construcción de manera tal que en 1922 se concluyeron definitivamente.',
          banner: 'http://127.0.0.1:5000/uploads/ca.jpg'
        }
      ]);
    });
};
