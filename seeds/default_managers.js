
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('managers').del()
    .then(function () {
      // Inserts seed entries
      return knex('managers').insert([
        { name: 'Monseñor Jorge Solórzano Pérez', number_phone: '5527235', id_churches: '1' },
        { name: 'Augusto Rivas', number_phone: '5522344', id_churches: '2' },
        { name: 'José Omar Cordero G.', number_phone: '5524091', id_churches: '3' },
        { name: 'Sergio A. Hernández O.', number_phone: '5524663', id_churches: '4' },
        { name: 'Rafael Umaña', number_phone: '5524026', id_churches: '6' },
        { name: 'Guillermo Blandón', number_phone: '5527840', id_churches: '8' }
      ]);
    });
};
