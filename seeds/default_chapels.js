
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('chapels').del()
    .then(function () {
      // Inserts seed entries
      return knex('chapels').insert([
        { name: 'Inmaculada Concepción', direction: 'Parque Central de Granada', id_churches: '1'},
        { name: 'Virgen del perpetuo Socorro', direction: 'Parque Central de Granada', id_churches: '1'},
        { name: 'Santísimo Sacramento', direction: 'Parque Central de Granada', id_churches: '1'},
        { name: 'Sacristía', direction: 'Parque Central de Granada', id_churches: '1'},
        { name: 'Santísimo', direction: 'Calle Real Xalteva, Granada', id_churches: '2'},
        { name: 'Virgen de Fátima', direction: 'Calle Real Xalteva, Granada', id_churches: '4'},
        { name: 'La virgen de Nuestras Señora del Rosario', direction: 'Calle Real Xalteva, Granada', id_churches: '5'}
      ]);
    });
};
