
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('services').del()
    .then(function () {
      // Inserts seed entries
      return knex('services').insert([
        { description: 'Misa' },
        { description: 'Adoración al Santísimo' },
        { description: 'Confesiones' },
        { description: 'Bautismo' },
        { description: 'Boda' }
      ]);
    });
};
