exports.up = function(knex) {
    return knex.schema.createTable("churches", table => {
        table.increments("id").primary();
        table.text("name").notNullable();
        table.text("location").notNullable();
        table.text("direction").notNullable();
        table.text("schedule_mass").notNullable();
        table.integer("construction_year").notNullable();
        table.text("territorial_expansion").notNullable();
        table.text("history").notNullable();
        table.text("banner").notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("chucches");
};
