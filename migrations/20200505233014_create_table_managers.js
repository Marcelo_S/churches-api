exports.up = function(knex) {
    return knex.schema.createTable("managers", table => {
        table.increments("id").primary();
        table.text("name").notNullable();
        table.integer("number_phone").notNullable();
        table.integer("id_churches").notNullable();

    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("managers");
};
