exports.up = function(knex) {
    return knex.schema.createTable("chapels", table => {
        table.increments("id").primary();
        table.text("name").notNullable();
        table.text("direction").notNullable();
        table.integer("id_churches").notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("chapels");
};