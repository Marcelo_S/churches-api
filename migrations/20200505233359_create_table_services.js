exports.up = function(knex) {
    return knex.schema.createTable("services", table => {
        table.increments("id").primary();
        table.text("description").notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("services");
};
