exports.up = function(knex) {
    return knex.schema.createTable("church_services", table => {
        table.increments("id").primary();
        table.integer("id_churches").notNullable();
        table.integer("id_services").notNullable();
    })
};

exports.down = function(knex) {
    return knex.schema.dropTableIfExists("church_services");
};
