const { Model } = require('objection');

class Services extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'services'
    }

    static get jsonSchema(){
        return {
            type: 'object',
            require: ['description'],

            properties: {
                description:  { type: 'string' }
            }
        }
    }

    fetchAll = async () => {
        try {
            const result = await Services.query();
            return result;
        } catch (error) {
            console.log({
                error
            });
        }

    }

    find = async (id) => {
        const result = await Services.query().findById(id);

        return result;
    }
}

module.exports = Services;