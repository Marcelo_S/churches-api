const { Model } = require('objection');

class Chapels extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'chapels'
    }

    static get jsonSchema(){
        return {
            type: 'object',
            require: ['name', 'direction'],

            properties: {
                name:  { type: 'string' },
                direction:  { type: 'string' }
            }
        }
    }

    static get relationMappings(){
        const Churches = require('./churchesModel');

        return {
            churches: {
                relation: Model.BelongsToOneRelation,
                modelClass: Churches,

                join: {
                    from: 'chapels.id_churches',
                    to: 'churches.id'
                }
            }
        }
    }

    fetchAll = async () => {
        const result = await Chapels.query().withGraphFetched('churches');
        return result;
    }

    find = async (id) => {
        const result = await Chapels.query().findById(id).withGraphFetched('churches');
        return result;
    }
}

module.exports = Chapels;