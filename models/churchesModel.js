const { Model } = require('objection');

class Churches extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'churches'
    }

    static get jsonSchema(){
        return {
            type: 'object',
            require: ['description','name','location','direction','schedule_maass','construction_year','territorial_expansion','history'],

            properties: {
                description:  { type: 'string' },
                name: { type: 'string' },
                location: { type: 'string' },
                direction: { type: 'string' },
                schedule_maass: { type: 'string' },
                construction_year: { type: 'integer' },
                territorial_expansion: { type: 'string' },
                history: { type: 'string' },
            }
        }
    }

    static get relationMappings(){
        const Services = require('./services');
        const Managers = require('./managersModel');
        const Chapels = require('./chapelsModel');
        
        return {
            services: {
                relation: Model.ManyToManyRelation,
                modelClass: Services,
                join: {
                    from: "churches.id",
                    through:{
                        from: "church_services.id_churches",
                        to: "church_services.id_services",
                    },
                    to: "services.id"
                }
            },
            managers: {
                relation: Model.HasManyRelation,
                modelClass: Managers,

                join: {
                    from: "churches.id",
                    to: "managers.id_churches"
                }
            },
            chapels: {
                relation: Model.HasManyRelation,
                modelClass: Chapels,

                join: {
                    from: "churches.id",
                    to: "chapels.id_churches"
                }
            }
        }
    }

    fetchAll = async () => {
        const result = await Churches.query().withGraphFetched({ services: true, managers: true, chapels: true  });
        return result;
    }

    find = async (id) => {
        const result = await Churches.query().findById(id).withGraphFetched({ services: true, managers: true, chapels: true  });

        return result;
    }
}

module.exports = Churches;