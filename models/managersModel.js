const { Model } = require('objection');

class Managers extends Model{
    static get idColumn(){
        return 'id';
    }

    static get tableName(){
        return 'managers'
    }

    static get jsonSchema(){
        return {
            type: 'object',
            require: ['name', "number_phone", "id_churches"],

            properties: {
                name:  { type: 'string' },
                number_phone:  { type: 'integer' },
                id_churches:  { type: 'integer' },
            }
        }
    }

    static get relationMappings(){
        const Churches = require('./churchesModel');

        return {
            churche : {
                relation: Model.BelongsToOneRelation,
                modelClass: Churches,

                join: {
                    from: "managers.id_churches",
                    to: "churches.id"
                }
            }
        }
    }

    fetchAll = async () => {
        try {
            const result = await Managers.query().withGraphFetched('churche');
            return result;
        } catch (error) {
            console.log({
                error
            });
        }

    }

    find = async (id) => {
        const result = await Managers.query().findById(id).withGraphFetched('churche');

        return result;
    }
}

module.exports = Managers;