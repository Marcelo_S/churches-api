const express = require("express");
const bodyParse = require("body-parser");
const dotEnv = require("dotenv");
const morgan = require("morgan");

//objection
const knexConfig = require('./knexfile');
const Knex = require('knex');
const { Model } = require('objection');

const servicesController = require('./controllers/servicesController');
const churchesController = require('./controllers/churchesController');
const managersController = require('./controllers/managersController');
const chapelsController = require('./controllers/chapelsController');

dotEnv.config({path: "./config.env"});

const app = express();

app.use(morgan('dev'));
app.use(bodyParse.json());
app.use(bodyParse.urlencoded({extend:false}));

//objection configuration
const knex = Knex(knexConfig.development);
Model.knex(knex);

//route
app.use('/services', servicesController);
app.use('/churches', churchesController);
app.use('/managers', managersController);
app.use('/chapels', chapelsController);


//Makes uploads folder static
app.use('/uploads', express.static('uploads'));

app.use((req, res) => {
    res.status(404).send({ error: 'page not found' });
});
//errors
app.use((err, req, res) => {
    console.log(err,message);
    res.status(err.status || 500);
    res.send({
        message: err.message,
        errors: err.errors,
    });
});

app.listen(process.env.PORT || 3000, _ => {
    console.clear();
    console.log(`Server is running on port ${process.env.PORT}`);
});