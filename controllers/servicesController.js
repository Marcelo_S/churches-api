const express = require('express');
const Services = require('../models/services');
const asyncWrapper = require('../middleware/asyncWrap');

const router = express.Router();

router.get("/", asyncWrapper(async (req, res) => {
    const services = new Services();

    const result = await services.fetchAll();
    
    if(result.length){
        return res.status(200).send(result);
    }

    throw({
        code: 400,
        message: "record not found"
    });
}));


router.get("/:id", asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const services = new Services();

    const result = await services.find(id);

    if(result){
        return res.status(200).send(result);
    }

    throw({
        code: 400,
        message: "record not found"
    });
}));

module.exports = router;