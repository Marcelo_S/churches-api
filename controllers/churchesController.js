const express = require('express');
const Churches = require('../models/churchesModel');
const asyncWrapper = require('../middleware/asyncWrap');

const router = express.Router();

router.get("/", asyncWrapper(async (req, res) => {
    const churches = new Churches();

    const result = await churches.fetchAll();
    
    if(result.length){
        return res.status(200).send(result);
    }

    throw({
        code: 400,
        message: "record not found"
    });
}));


router.get("/:id", asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const churches = new Churches();

    const result = await churches.find(id);

    if(result){
        return res.status(200).send(result);
    }

    throw({
        code: 400,
        message: "record not found"
    });
}));

module.exports = router;