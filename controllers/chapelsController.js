const express = require('express');
const Chapels = require('../models/chapelsModel');
const asyncWrapper = require('../middleware/asyncWrap');

const router = express.Router();

router.get("/", asyncWrapper(async (req, res) => {
    const chapels = new Chapels();

    const result = await chapels.fetchAll();
    
    if(result.length){
        return res.status(200).send(result);
    }

    throw({
        code: 400,
        message: "record not found"
    });
}));


router.get("/:id", asyncWrapper(async (req, res) => {
    const { id } = req.params;
    const chapels = new Chapels();

    const result = await chapels.find(id);

    if(result){
        return res.status(200).send(result);
    }

    throw({
        code: 400,
        message: "record not found"
    });
}));

module.exports = router;